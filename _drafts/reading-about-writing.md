---
title: Reading About Writing
description:
# type: note article
tags: ['Writing']
last_modified_at: 2021-10-13
date: 2021-09-29
---

## Dreyer's English: An Utterly Correct Guide to Clarity and Style <span>by Benjamin Dreyer</span>

This is mostly a discussion of how Dreyer sees the rules of writing. How he thinks about grammar rules, pumctuation, etc. He uses "one" so often it's hard to read at times. He also uses a lot of complicated sentence strutures, making it hard to read --- ironic for a "how to write" guide. Dreyer tries to bring humor to grammar and word choice --- it works, mostly. I skipped the chapters on "easily misspelled words," "peeves and crotchets," "confusables," "proper nouns," and "the trimmables."

## The Situation and the Story <span>by Vivian Gornick</span>

This has a promising title. I figured Gornick would give at least one example of a situation versus a story. If that information is in this book, I can't find it. In fact, William Zinsser had more to say on the subject in his American Scholar story [How to Write a Memoir](https://theamericanscholar.org/how-to-write-a-memoir/):

> Remember: Your biggest stories will often have less to do with their subject than with their significance—not what you did in a certain situation, but how that situation affected you and shaped the person you became.
