---
title:
description:
# type: note article
tags:
last_modified_at: 2021-09-05
date: 2021-09-05
---

I often hear how "we're all subscribed to too many things."

But, I'm not.

In fact, I have the opposite problem. I can't find enough good things to subscribe to. I find many newsletters and blogs that could likely be outdone by a smart dog or an average chimpanzee. For an example see the Huffington Post or BuzzFeed or Medium.

If you are subscribed to too many things, there is an unsubscribe button.
