---
title: Brad's Wishlist
permalink: /wishlist/
no_index: true
---

- Webster's New International Dictionary: Second Edition Unabridged (1934)

  This edition has been out of print since 1961. I have been looking for years and have not found one that isn't very expensive or in total disrepair.

- Raspberry Pi

  Since these are unavalible, an alternative such as the [Libre Computer Board](https://www.amazon.com/Libre-Computer-AML-S905X-CC-Potato-64-bit/dp/B074P6BNGZ) would be just as good. A [case](https://www.amazon.com/iUniker-Raspberry-Cooling-Heatsink-Removable/dp/B079M96KWZ) would also be good.

<!-- - Electric Razor -->

<!-- - watch -->
