---
title: How I Find Interesting Websites
type: note
description:
tags: []
layout: article
began_drafting: 2021-09-16
last_modified: 2023-04-02
---

I emailed a link to a friend which prompted him to ask "how do you find so many interesting things on the internet?"

I start with curated newsletters. My favorite is [Hacker Newsletter](https://hackernewsletter.com/). Emily Ding's [Landmarkings](https://movableworlds.co/s/landmarkings/archive) is good. So is Austin Kleon's [10 things worth sharing this week](https://austinkleon.substack.com/archive).

The flow looks like this:

If there is a [Now page](https://nownownow.com/about), I read it. If there is an About page, I read it. If there is a Start Here page, I read it. After that I visit the homepage. What I'm looking for is someone who looks to be publishing interesting things. If I find a couple articles that look promising, I'll save them to read later. If those articles prove to be interesting, I'll add the site to my [RSS reader](/web-feeds/).

An example from a recent sleuthing: [Gergely Orosz](https://blog.pragmaticengineer.com/) has written a blog since 2015; he's authored three books; he [started a newsletter](https://newsletter.pragmaticengineer.com/p/coming-soon) in 2021. I wanted to find if Gergely had written anything about the publishing of his books. I started with this google search `site:blog.pragmaticengineer.com writing books`. That lead me to his article called [Undervalued Software Engineering Skills: Writing Well](https://blog.pragmaticengineer.com/on-writing-well/). In that article he linked to a [tweet](https://twitter.com/GergelyOrosz/status/1353661833236926470) where he listed a few authors talking about writing.

- [It's time to start writing](https://alexnixon.github.io/2019/12/10/writing.html) by Alex Nixon
- [Writing is Thinking](https://blog.stephsmith.io/learning-to-write-with-confidence/) by Steph Smith
- [Writing Is Thinking](https://boz.com/articles/writing-thinking) by Andrew Bosworth

I didn't find what I was looking for on Gergely's site, but I did find three new blogs to read.
