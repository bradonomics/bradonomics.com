---
title: Building Networks
type: quote
---

<blockquote>
  <p>The richest people in the world look for and build networks, everyone else looks for work.</p>
  <p class="cite">—Robert Kiyosaki</p>
</blockquote>
