---
title: Take to the Oars
type: quote
---

<blockquote>
  <p>If the wind will not serve, take to the oars.</p>
  <p class="cite">—Latin Proverb</p>
</blockquote>
