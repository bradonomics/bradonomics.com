---
title: Your Life Sucks
type: quote
---

<blockquote>
  <p>Remember, Mondays are fine. It's your life that sucks.</p>
  <p class="cite">—Ricky Gervais</p>
</blockquote>
