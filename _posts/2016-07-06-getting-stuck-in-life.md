---
title: Getting Stuck in Life
type: note
---

Joe Rogan on getting stuck in life:

<div class="video-wrapper">
  <iframe title="Getting Stuck in Life" src="https://www.youtube-nocookie.com/embed/e8jIZ3NB7s4?rel=0" frameborder="0" allowfullscreen></iframe>
</div>
