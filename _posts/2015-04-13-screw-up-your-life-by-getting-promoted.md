---
title: How to Screw Up Your Life by Getting Promoted
type: quote
---

<blockquote>
  <p>Building something interesting requires a surplus of time and money. Salaried jobs provide neither.</p>
  <p class="cite">—<a href="https://web.archive.org/web/20200220141023/http://thestartuptoolkit.com/blog/2012/12/how-to-screw-up-your-life-by-getting-promoted/">Rob Fitzpatrick</a></p>
</blockquote>
