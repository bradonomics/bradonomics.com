---
title: Gardens and Libraries
type: quote
---

<blockquote>
  <p>If you have a garden and a library, you have everything you need.</p>
  <p class="cite">—Cicero</p>
</blockquote>
